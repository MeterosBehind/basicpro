package mysql;

import javafx.application.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class MySQLConnector {
    private final static Logger logger = LoggerFactory.getLogger(Application.class);
    public Connection getConnection(){
        Properties pro = new Properties();
        try{
            pro.load(this.getClass().getClassLoader().getResourceAsStream("DBConfig.properties"));
            String driver = pro.getProperty("driver");
            String url = pro.getProperty("url");
            String username = pro.getProperty("username");
            String paw = pro.getProperty("password");
            Class.forName(driver);
            Connection connection = DriverManager.getConnection(url,username,paw);
            connection.setAutoCommit(false);
            return connection;
        }catch (Exception e){
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        return null;
    }
}
