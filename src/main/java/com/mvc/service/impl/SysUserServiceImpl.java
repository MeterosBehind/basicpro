package com.mvc.service.impl;

import com.mvc.dao.mapper.SysUserMapper;
import com.mvc.entity.SysUser;
import com.mvc.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    SysUserMapper sysUserMapper;

    public SysUser getSysUsers(int userId){
        return sysUserMapper.selectByPrimaryKey1(userId);
    }
}
