package com.mvc.service;

import com.mvc.entity.SysUser;

import java.util.List;

public interface SysUserService {
    public SysUser getSysUsers(int userId);
}
