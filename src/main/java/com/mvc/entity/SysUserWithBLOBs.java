package com.mvc.entity;

public class SysUserWithBLOBs extends SysUser {
    private byte[] userPicData;

    private String about;

    public byte[] getUserPicData() {
        return userPicData;
    }

    public void setUserPicData(byte[] userPicData) {
        this.userPicData = userPicData;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about == null ? null : about.trim();
    }
}