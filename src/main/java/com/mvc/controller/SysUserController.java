package com.mvc.controller;

import com.mvc.entity.SysUser;
import com.mvc.service.SysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

@Controller
public class SysUserController {
    private static final Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "/user/getUser",method = RequestMethod.GET)
    public ModelAndView getUser(Integer userId){
        ModelAndView modelAndView = new ModelAndView();
        SysUser sysUser = sysUserService.getSysUsers(userId);
        modelAndView.addObject("users",sysUser);
        modelAndView.setViewName("SysUserModel");
        logger.info("===============================成功查询用户列表:{}",sysUser.getOrgId()+sysUser.getUsername());
        return modelAndView;
    }

    @RequestMapping(value = "/user/getUser1",method = RequestMethod.GET)
    public ModelAndView getUser1(Integer userId){
        ModelAndView modelAndView = new ModelAndView();
        SysUser sysUser = sysUserService.getSysUsers(userId);
        modelAndView.addObject("users",sysUser);
        logger.info("===============================成功查询用户列表1:{}",sysUser.getOrgId()+sysUser.getUsername());
        return modelAndView;
    }

    @RequestMapping(value = "/user/getUser2",method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String,Object> getUser2(Integer userId){
        HashMap<String,Object> map = new HashMap<>();
        SysUser sysUser = sysUserService.getSysUsers(userId);
        map.put("result","success");
        map.put("userInfo",sysUser);
        logger.info("===============================成功查询用户列表1:{}",sysUser.getOrgId()+sysUser.getUsername());
        return map;
    }

    @RequestMapping(value = "/user/getUser3",method = RequestMethod.GET)
    public String getUser3(Integer userId){
        HashMap<String,Object> map = new HashMap<>();
        SysUser sysUser = sysUserService.getSysUsers(userId);
        map.put("result","success");
        map.put("userInfo",sysUser);
        logger.info("===============================成功查询用户列表1:{}",sysUser.getOrgId()+sysUser.getUsername());
        return "../views/index";
    }



}
